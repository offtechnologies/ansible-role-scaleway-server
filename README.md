ansible-role-scaleway-server
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-scaleway-server/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-scaleway-server/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]


Manages compute instance on your Scaleway account.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```shell
export SCW_TOKEN="My Super Secret Scaleway OAuth Token"
export SCW_ACCESS_KEY="secret"
export SCW_TOKEN="secret"
export SCW_KEY="your scaleway_id_rsa.pub key"
```

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-scaleway-server }
```

License
-------

BSD
